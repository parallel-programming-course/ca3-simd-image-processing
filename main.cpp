#include <iostream>
#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include <math.h>
#include <sys/time.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include "x86intrin.h"

using namespace cv;
using namespace std;

long part1_serial(unsigned char *in_image1, unsigned char *in_image2, unsigned int NCOLS, unsigned int NROWS) {
	struct timeval start, end;

    // Create output image
	cv::Mat out_img (NROWS, NCOLS, CV_8U);

    // Serial implementation
	gettimeofday(&start, NULL);
	
	unsigned char *out_image = (unsigned char *) out_img.data;

	for (int row = 0; row < NROWS; row++)
		for (int col = 0; col < NCOLS; col++)
			*(out_image + row * NCOLS + col) = (*(in_image1 + row * NCOLS + col) - *(in_image2 + row * NCOLS + col) > 0) ? *(in_image1 + row * NCOLS + col) - *(in_image2 + row * NCOLS + col) : *(in_image2 + row * NCOLS + col) - *(in_image1 + row * NCOLS + col);

	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);

    printf("\nSerial:\n");
    printf("Time: %ld microseconds\n", micros);		

    cv::imwrite("part1_serial.png", out_img);
    
	return micros;
}

long part1_parallel(unsigned char *in_image1, unsigned char *in_image2, unsigned int NCOLS, unsigned int NROWS) {
	struct timeval start, end;

    // Create output image
	cv::Mat out_img(NROWS, NCOLS, CV_8U);

    // Parallel implementation
	gettimeofday(&start, NULL);

	__m128i *pSrc1;
	__m128i *pSrc2;
	__m128i *pRes;
	__m128i m1, m2, m3;

	pSrc1 = (__m128i *) in_image1;
	pSrc2 = (__m128i *) in_image2;
	pRes = (__m128i *) out_img.data;

	for (int i = 0; i < NROWS; i++) {
		for (int j = 0; j < NCOLS / 16; j++) {
			m1 = _mm_loadu_si128(pSrc1 + i * NCOLS/16 + j) ;
			m2 = _mm_loadu_si128(pSrc2 + i * NCOLS/16 + j) ;
			m3 = _mm_or_si128(_mm_subs_epu8(m1, m2), _mm_subs_epu8(m2, m1));
			_mm_storeu_si128 (pRes + i * NCOLS/16 + j, m3);
		}
	}
	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);

    printf("\nParallel:\n");
    printf("Time: %ld microseconds\n", micros);		

    cv::imwrite("part1_parallel.png", out_img);

	return micros;
}

long part2_serial_run(unsigned char *input_img_2_data, unsigned int NCOLS_1, unsigned int NCOLS_2, unsigned int NROWS_1, unsigned int NROWS_2) {
	struct timeval start, end;

    // Create output image
	cv::Mat out_img = cv::imread("CA03__Q2__Image__01.png", IMREAD_GRAYSCALE);

	unsigned char *out_img_data;
	out_img_data = (unsigned char *) out_img.data;

	// Serial implementation
	gettimeofday(&start, NULL);
    for (int row = 0; row < NROWS_2; row++)
		for (int col = 0; col < NCOLS_2; col++) {
			*(out_img_data + row * NCOLS_1 + col) += (*(input_img_2_data + row * NCOLS_2 + col) >> 1);
            if ( *(out_img_data + row * NCOLS_1 + col) < (*(input_img_2_data + row * NCOLS_2 + col) >> 1) )
                *(out_img_data + row * NCOLS_1 + col) = (unsigned char) 255;
        }
	
	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);

    printf("\nSerial:\n");
    printf("Time: %ld microseconds\n", micros);		

    cv::imwrite("part2_serial.png", out_img);

	return micros;
}

long part2_parallel_run(unsigned char *input_img_2_data, unsigned int NCOLS_1, unsigned int NCOLS_2, unsigned int NROWS_1, unsigned int NROWS_2) {
	struct timeval start, end;

	// Create output image
	cv::Mat out_img = cv::imread("CA03__Q2__Image__01.png", IMREAD_GRAYSCALE);

    // Parallel implementation
	gettimeofday(&start, NULL);

    __m128i *in2;
	__m128i *out;
	__m128i m1, m2, m3, m4;
	in2 = (__m128i *) input_img_2_data;
	out = (__m128i *) out_img.data;

    m3 = _mm_set1_epi8 ((unsigned char) 0X7F); // 16 * [0111 1111] (16 * 8 bit = 128 bit)
	for (int row = 0; row < NROWS_2; row++)
		for (int col = 0; col < NCOLS_2 / 16; col++)
		{
			m1 = _mm_loadu_si128(out + row * NCOLS_1/16 + col);
            m2 = _mm_loadu_si128(in2 + row * NCOLS_2/16 + col) >> 1;
			m2 = _mm_and_si128(m2, m3);
            m4 = _mm_adds_epu8(m1, m2);
			_mm_storeu_si128(out + row * NCOLS_1/16 + col, m4);
		}

	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);

    printf("\nParallel:\n");
    printf("Time: %ld microseconds\n", micros);		

    cv::imwrite("part2_parallel.png", out_img);

	return micros;
}

float part1() {
	cv::Mat img1 = cv::imread("CA03__Q1__Image__01.png", IMREAD_GRAYSCALE);
	cv::Mat img2 = cv::imread("CA03__Q1__Image__02.png", IMREAD_GRAYSCALE);
    unsigned char *in_image1 = (unsigned char *) img1.data;
	unsigned char *in_image2 = (unsigned char *) img2.data;
	unsigned int NCOLS = img1.cols;
	unsigned int NROWS = img1.rows;
	long sTime = part1_serial(in_image1, in_image2, NCOLS, NROWS);
	long pTime = part1_parallel(in_image1, in_image2, NCOLS, NROWS);
    return float(sTime)/float(pTime);
}

float part2() {
    
    // LOAD image
	cv::Mat input_img_1 = cv::imread("CA03__Q2__Image__01.png", IMREAD_GRAYSCALE);
	cv::Mat input_img_2 = cv::imread("CA03__Q2__Image__02.png", IMREAD_GRAYSCALE);
	unsigned char *input_img_1_data  = (unsigned char *) input_img_1.data;
	unsigned char *input_img_2_data  = (unsigned char *) input_img_2.data;

	unsigned int NCOLS_1 = input_img_1.cols;
	unsigned int NROWS_1 = input_img_1.rows;
    unsigned int NCOLS_2 = input_img_2.cols;
	unsigned int NROWS_2 = input_img_2.rows;

    long sTime = part2_serial_run(input_img_2_data, NCOLS_1, NCOLS_2, NROWS_1, NROWS_2);
	long pTime = part2_parallel_run(input_img_2_data, NCOLS_1, NCOLS_2, NROWS_1, NROWS_2);
	return float(sTime)/float(pTime);
}

int main() { 
	printf("MirHamed Jafarzadeh Asl: 810096008\n");
	printf("Seyede Mehrnaz Shamsabadi: 810196493\n");

	printf("**************************************\n");

	printf("Part 1:\n");
	float speed_up_part1 = part1();
	printf("\nSpeed Up of Part 1: %f\n", speed_up_part1);

	printf("**************************************\n");

	printf("Part 2:\n");
	float speed_up_part2 = part2();
	printf("\nSpeed Up of Part 2: %f\n", speed_up_part2);
	
	return 0; 
}